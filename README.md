# Entity
Entity of Golite ecosystem.

## Test coverage
- generator.go - 90%

## Installation
```shell
go get "gitlab.com/golight/entity"
```
## Available functions

### ReflectFile
```go
// ReflectFile inspects the AST of a Go file and returns a slice of ReflectedStruct,
// which contain information about each struct found in the file along with its fields and methods.
// An error is returned if parsing fails.
func ReflectFile(fileName string) ([]ReflectedStruct, error)

// ReflectedStruct encapsulates metadata about a struct.
// It includes the StructName, whether HasDBTag is set, and slices for Fields and Methods.
type ReflectedStruct struct {
    StructName string
    HasDBTag   bool
    Fields     []FieldInfo
    Methods    []MethodInfo
}

// MethodInfo holds details about a method within a struct.
// It includes the Receiver's name and the Name of the method.
type MethodInfo struct {
    Receiver string
    Name     string
}

// FieldInfo contains information about a field in a struct.
// Each field has a Name representing the field's name and a Type representing the field's type.
type FieldInfo struct {
    Name string
    Type string
}

```

```go
// GenerateMethods generates method definitions for structs with a DB tag in the given file.
// It returns a string containing the generated methods.
func GenerateMethods(filepath string) string
```

```go
// GenerateMethodsToFile appends the generated methods to the specified file path.
// Any error during the file operation is logged.
func GenerateMethodsToFile(filepath string)
```

## Usage example

```go
package main

import (
    "gitlab.com/golight/entity/generator"
)

func main() {
    generator.GenerateMethodsToFile("./examples/example.go")
}


```
