package main

import (
	"gitlab.com/golight/entity/generator"
)

func main() {
	generator.GenerateMethodsToFile("./examples/example.go")
}
