package main

import "time"

type Example struct {
	Example1 string    `db:"example_1" db_index:"index"`
	Example2 int       `db:"example_2"`
	Example3 bool      `db:"example_3"`
	Example4 time.Time `db:"example_4"`
}

func (e *Example) TableName() string {
	return "examples"
}

func (e *Example) OnCreate() []string {
	return []string{}
}

func (e *Example) FieldsPointers() []interface{} {
	return []interface{}{
		&e.Example1,
		&e.Example2,
		&e.Example3,
		&e.Example4,
	}
}
